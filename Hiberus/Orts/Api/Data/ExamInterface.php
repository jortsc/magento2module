<?php
namespace Hiberus\Orts\Api\Data;

/**
 * @author: Jose Manuel Orts
 * @date: 21/07/2020
 */

interface ExamInterface
{
    /**
     * @return int
    */
    public function getExamId();

    /**
     * @param int $examId
     * @return $this
     */
    public function setExamId(int $examId);

    /**
     * @return string
     */
    public function getFirstName();

    /**
     * @param String $firstName
     * @return $this
     */
    public function setFirstName(string $firstName);

    /**
     * @return string
     */
    public function getLastName();

    /**
     * @param String $lastName
     * @return $this
     */
    public function setLastName(string $lastName);

    /**
     * @return float
     */
    public function getMark();

    /**
     * @param float $mark
     * @return $this
     */
    public function setMark(float $mark);
}
