<?php
namespace Hiberus\Orts\Block;

use Hiberus\Orts\Api\ExamRepositoryInterface;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;

/**
 * @author: Jose Manuel Orts
 * @date: 21/07/2020
 */

class Course extends Template
{
    /**
     * @var ExamRepositoryInterface
    */
    private ExamRepositoryInterface $examRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private SearchCriteriaBuilder $searchCriteriaBuilder;
    /**
     * @var SortOrderBuilder
     */
    private SortOrderBuilder $sortOrderBuilder;
    /**
     * @var CollectionFactory
    */
    private CollectionFactory  $categoryCollection;
    /**
     * @var CategoryInterface
     */
    private CategoryInterface $catalogCategory;

    public function __construct(
        Context $context,
        CollectionFactory $categoryCollection,
        CategoryInterface $catalogCategory
    ) {
        $this->categoryCollection = $categoryCollection;
        $this->catalogCategory = $catalogCategory;

        parent::__construct($context);
    }


    public function getRootCategoriesByStore()
    {
        $storesList = $this->_storeManager->getStores();

        $stores = [];
        foreach ($storesList as $store) {
            $rootCategoryId = $store->getRootCategoryId();
            $collection = $this->catalogCategory->load($rootCategoryId);

            $stores[] = [
                'storeName' => $store->getName(),
                'name' => $collection->getName(),
                'id' => $collection->getId(),
            ];
        }

        return $stores;
    }

    public function getCategories()
    {
        return $this->categoryCollection->create()
            ->addOrderField('name')
            ->addIsActiveFilter();
    }
}
