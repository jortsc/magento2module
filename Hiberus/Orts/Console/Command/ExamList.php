<?php
namespace Hiberus\Orts\Console\Command;

use Hiberus\Orts\Api\ExamRepositoryInterface;
use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Api\SortOrderBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author: Jose Manuel Orts
 * @date: 22/07/2020
 */

class ExamList extends Command
{
    const QUANTITY = 'quantity';
    const COMMAND_NAME = 'hiberus:orts';

    /**
     * @var ExamRepositoryInterface $examRepository
    */
    private ExamRepositoryInterface $examRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @var SortOrderBuilder
     */
    private SortOrderBuilder $sortOrderBuilder;


    public function __construct(
        ExamRepositoryInterface $examRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder
    ) {
        $this->examRepository = $examRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;

        parent::__construct(self::COMMAND_NAME);
    }

    /**
     * php bin/magento hiberus:orts [--quantity="5"]
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription('Shows exams list');
        $this->addOption(
            self::QUANTITY,
            null,
            InputOption::VALUE_OPTIONAL,
            'Quantity'
        );
        parent::configure();
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $quantity = $input->getOption(self::QUANTITY);
        $_filter = $this->getCollectionFilter();

        $output->writeln('<question> Students with Mark </question>');
        $this->filterQuantity($quantity, $_filter, $output);

        $this->printList($_filter, $output);

        $output->writeln('<question> Process finished </question>');
    }

    /**
     * @return SearchCriteria
     */
    private function getCollectionFilter()
    {
        $_filter = $this->searchCriteriaBuilder->create();
        $sortOrder = $this->sortOrderBuilder->setField('firstname')
            ->setDirection(SortOrder::SORT_ASC)
            ->create();
        $_filter->setSortOrders([$sortOrder]);

        return $_filter;
    }

    /**
     * @param $quantity
     * @param SearchCriteria $_filter
     * @param OutputInterface $output
     */
    private function filterQuantity($quantity, SearchCriteria $_filter, OutputInterface $output)
    {
        if ($quantity) {
            $_filter->setPageSize($quantity);
            $output->writeln('');
        }
    }

    /**
     * @param SearchCriteria $_filter
     * @param OutputInterface $output
     */
    private function printList(SearchCriteria $_filter, OutputInterface $output)
    {
        $list = $this->examRepository->getList($_filter);
        foreach ($list->getItems() as $exam) {
            $output->writeln(
                "<info>{$exam->getLastName()}, {$exam->getFirstName()}: </info><comment>{$exam->getMark()}</comment>"
            );
        }
        $output->writeln('');
    }
}
