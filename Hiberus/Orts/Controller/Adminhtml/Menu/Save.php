<?php
namespace Hiberus\Orts\Controller\Adminhtml\Menu;

use Hiberus\Orts\Api\ExamRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\Session;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\Message\ManagerInterface;

/**
 * @author: Jose Manuel Orts
 * @date: 23/07/2020
 */
class Save implements ActionInterface
{
    const ADMIN_RESOURCE = 'Index';

    /**
     * @var Context
     */
    protected Context $context;
    /**
     * @var RedirectFactory
     */
    protected RedirectFactory $redirectFactory;
    /**
     * @var ExamRepositoryInterface
     */
    protected ExamRepositoryInterface $examRepository;
    /**
     * @var Session
    */
    protected Session $session;
    /**
     * @var ManagerInterface
     */
    protected ManagerInterface $messenger;

    public function __construct(
        Context $context,
        RedirectFactory $redirectFactory,
        ExamRepositoryInterface $examRepository,
        Session $session,
        ManagerInterface $messenger

    ) {
        $this->context = $context;
        $this->redirectFactory = $redirectFactory;
        $this->examRepository = $examRepository;
        $this->session = $session;
        $this->messenger = $messenger;
    }

    public function execute()
    {
        $data = $this->context->getRequest()->getPostValue();
        $data = array_filter($data, function ($value) {return $value !== '';});

        $exam = $this->examRepository->load(isset($data['id_exam']) ? $data['id_exam'] : null);
        $exam->setFirstName($data['firstname'])
            ->setLastName($data['lastname'])
            ->setMark($data['mark']);

        $this->examRepository->save($exam);

        $this->messenger->addSuccessMessage(__('Saved successfully.'));
        $this->session->setFormData(false);

        return $this->redirectFactory->create()->setPath('*/*/');
    }
}
