<?php
namespace Hiberus\Orts\Model;

use Hiberus\Orts\Api\Data\ExamInterface;
use Hiberus\Orts\Model\ResourceModel\ResourceExam;
use Magento\Framework\Model\AbstractModel;

/**
 * @author: Jose Manuel Orts
 * @date: 21/07/2020
 */

class Exam extends AbstractModel implements ExamInterface
{
    protected $_eventPrefix = 'hiberus_orts_exam';

    /**
     * Legacy constructor
    */
    protected function _construct()
    {
        $this->_init(ResourceExam::class);
    }

    public function getExamId()
    {
        return $this->_getData('id_exam');
    }

    public function setExamId(int $examId)
    {
        $this->setData('id_exam', $examId);

        return $this;
    }

    public function getFirstName()
    {
        return $this->_getData('firstname');
    }

    public function setFirstName($firstName)
    {
        $this->setData('firstname', $firstName);

        return $this;
    }

    public function getLastName()
    {
        return $this->_getData('lastname');
    }

    public function setLastName($lastName)
    {
        $this->setData('lastname', $lastName);

        return $this;
    }

    public function getMark()
    {
        return $this->_getData('mark');
    }

    public function setMark($mark)
    {
        $this->setData('mark', $mark);

        return $this;
    }
}
