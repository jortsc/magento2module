<?php
namespace Hiberus\Orts\Model;

use Hiberus\Orts\Api\Data\ExamInterface;
use Hiberus\Orts\Api\Data\ExamSearchResultsInterface;
use Hiberus\Orts\Api\ExamRepositoryInterface;
use Hiberus\Orts\Model\ResourceModel\Exam\Collection;
use Hiberus\Orts\Model\ResourceModel\Exam\CollectionFactory;
use Hiberus\Orts\Model\ResourceModel\ResourceExam;
use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Model\AbstractModel;
use Hiberus\Orts\Api\Data\ExamInterfaceFactory as ExamDataObjectFactory;

/**
 * @author: Jose Manuel Orts
 * @date: 21/07/2020
 */

class ExamRepository implements ExamRepositoryInterface
{
    /**
     * @var ExamFactory
     */
    protected ExamFactory $examFactory;
    /**
     * @var CollectionFactory
     */
    protected CollectionFactory $examCollectionFactory;
    /**
     * @var ResourceExam
    */
    protected ResourceExam $examResource;
    /**
     * @var ExamSearchResultsInterface
     */
    protected ExamSearchResultsInterface $searchResults;
    /**
     * @var ResourceModel\Exam\Collection
     */
    protected ResourceModel\Exam\Collection $collection;
    /**
     * @var ExamDataObjectFactory
     */
    protected ExamDataObjectFactory $examDataObjectFactory;

    public function __construct(
        ExamFactory $exam,
        CollectionFactory $collectionFactory,
        ResourceExam $resourceExam,
        ExamSearchResultsInterface $examSearchResults,
        ExamDataObjectFactory $examDataObjectFactory
    ) {
        $this->examFactory = $exam;
        $this->examResource = $resourceExam;
        $this->examCollectionFactory = $collectionFactory;
        $this->searchResults = $examSearchResults;
        $this->examDataObjectFactory = $examDataObjectFactory;
    }

    public function save(ExamInterface $exam): ExamInterface
    {
        $this->examResource->save($exam);

        return $exam;
    }

    public function delete(ExamInterface $exam): bool
    {
        try {
            $this->examResource->delete($exam);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__('Could not delete the exam.'), $e);
        }
        return true;
    }

    public function getList(SearchCriteria $searchCriteria): ExamSearchResultsInterface
    {
        $this->collection = $this->examCollectionFactory->create();
        $this->getSearchResultsCollection($searchCriteria);
        return $this->searchResults->setItems(
            $this->turnModelObjectIntoDataObject()
        );
    }

    /**
     * @return float
     * @throws \Zend_Db_Statement_Exception
     */
    public function getHighestMark(): float
    {
        return (float) $this->examResource->getConnection()
            ->query(
                'SELECT MAX(mark) FROM hiberus_exam'
            )->fetchColumn();
    }

    /**
     * @return float
     * @throws \Zend_Db_Statement_Exception
     */
    public function getAverage(): float
    {
        return (float) $this->examResource->getConnection()
            ->query(
                'SELECT AVG(mark) FROM hiberus_exam'
            )
            ->fetchColumn();
    }

    /**
     * @param $id
     * @return ExamInterface | AbstractModel
     */
    public function load($id): ExamInterface
    {
        $exam = $this->examFactory->create();
        $this->examResource->load($exam, $id, 'id_exam');
        return $exam;
    }

    /**
     * @param SearchCriteria $searchCriteria
     * @return ExamSearchResultsInterface
     */
    public function getArrayList(SearchCriteria $searchCriteria): ExamSearchResultsInterface
    {
        $items = [];
        $this->collection = $this->examCollectionFactory->create();
        $this->getSearchResultsCollection($searchCriteria);

        foreach ($this->collection as $object) {
            $items[] = $object->toArray();
        }
        return $this->searchResults->setItems($items);
    }

    /**
     * @param SearchCriteria $searchCriteria
     * @return ExamRepositoryInterface
     */
    protected function getSearchResultsCollection(SearchCriteria $searchCriteria): ExamRepositoryInterface
    {
        $this->searchResults->setSearchCriteria($searchCriteria);
        $this->setFilters($searchCriteria);
        $this->searchResults->setTotalCount($this->collection->getSize());
        $this->setSortOrders($searchCriteria, $this->collection);
        $this->collection->setCurPage($searchCriteria->getCurrentPage());
        $this->collection->setPageSize($searchCriteria->getPageSize());

        return $this;
    }

    /**
     * @param SearchCriteria $searchCriteria
     * @param Collection $collection
     */
    protected function setSortOrders(SearchCriteria $searchCriteria, Collection $collection): void
    {
        $sortOrders = $searchCriteria->getSortOrders();

        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
    }

    /**
     * @param SearchCriteria $searchCriteria
     */
    protected function setFilters(SearchCriteria $searchCriteria): void
    {
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $fields[] = $filter->getField();
                $conditions[] = [$condition => $filter->getValue()];
            }
            if ($fields) {
                $this->collection->addFieldToFilter($fields, $conditions);
            }
        }
    }

    /**
     * @return array
     */
    public function turnModelObjectIntoDataObject(): array
    {
        $items = [];
        /**@var ExamInterface $modelObject */
        foreach ($this->collection as $modelObject) {
            /** Data Object */
            $dataObject = $this->examDataObjectFactory->create();
            $dataObject->setExamId($modelObject->getExamId())
                ->setFirstName($modelObject->getFirstName())
                ->setLastName($modelObject->getLastName())
                ->setMark($modelObject->getMark());

            $items[] = $dataObject;
        }
        return $items;
    }
}
