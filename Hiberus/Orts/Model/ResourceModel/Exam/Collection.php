<?php
namespace Hiberus\Orts\Model\ResourceModel\Exam;

use Hiberus\Orts\Model\Exam;
use Hiberus\Orts\Model\ResourceModel\ResourceExam;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * @author: Jose Manuel Orts
 * @date: 21/07/2020
 */

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id_exam';
    protected $_eventPrefix = 'ad_shipping_quote_collection';
    protected $_eventObject = 'quote_collection';

    protected function _construct()
    {
        $this->_init(
            Exam::class,
           ResourceExam::class
        );
    }
}
