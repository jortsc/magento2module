<?php
namespace Hiberus\Orts\Plugin;

use Hiberus\Orts\Logger\Logger;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Webapi\Controller\Rest;


/**
 * @author: Jose Manuel Orts
 * @date: 22/07/2020
 */
class RestApiLog
{
    const MODULE_REST_PATH = '/rest/V1/hiberus-orts';
    /**
     * @var Logger $logger
    */
    private Logger $logger;

    /**
     * RestApiLog constructor.
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param Rest $subject
     * @param $result
     * @return null
     */
    public function beforeDispatch(Rest $subject, RequestInterface $request)
    {
        if ($this->isModuleRequest($request)) {
            $this->logger->debug(print_r($request->getPathInfo(), true));
            $this->logger->debug(print_r($request->getParams(), true));
        }
        return null;
    }

    /**
     * @param Rest $subject
     * @param ResponseInterface $response
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function afterDispatch(Rest $subject, ResponseInterface $response, RequestInterface $request): ResponseInterface
    {
        if ($this->isModuleRequest($request)) {
            $this->logger->debug(print_r($response->getBody(), true));
        }
        return $response;
    }

    private function isModuleRequest(RequestInterface $request): bool
    {
        return $request->getPathInfo() === self::MODULE_REST_PATH;
    }
}
