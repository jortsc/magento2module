# Magento 2 Custom Example Module
Author: jortsc@gmail.com

The module implements Service Contracts pattern for Data and Services such Data Entity and Repository and Management Services.

Module featured points:
* Frontend list
* Less as styling language 
* Admin CRUD with Ui Component `Grid` and `Form`
* Rest API
* Plugin
* Command
* Setup
* Test `Unit` and `Integration`

### Magento installation

```
 composer create-project --repository-url=https://repo.magento.com/ magento/project-community-edition magento2
```

### Install module as a composer package
To do not mix our code with Magento's code we can work in any directory at least until we publish it from the repo url, and load it to Magento through Composer. It will be loaded with its own namespace not mixing with Magento's module- folders.   
```
composer config repositories.orts path custom/Hiberus/Orts
&& composer require hiberus/module-orts:0.1.0 
&& composer update
```

### Developer Mode
```
php bin/magento deploy:mode:set developer
```

### Enable module
```
php bin/magento module:enable Hiberus_Orts
```

### Compile Code
```
php bin/magento setup:di:compile
```

### Setup Module
```
php ./bin/magento setup:upgrade
```

## Rest Api

Url: [hostUrl]/swagger#/hiberusOrtsEndpointsRestExamV1

![alt text](img/endpoints.png "Endpoints")

![alt text](img/get.png "Get")

![alt text](img/post.png "Post")

![alt text](img/delete.png "Delete")


### Curl Commands
Get
```
curl -k "https://mage.dev.local/rest/V1/hiberus-orts?searchCriteria[sortOrders][0][field]=firstname&searchCriteria[sortOrders][0][direction]=DESC&searchCriteria[currentPage]=1&searchCriteria[pageSize]=12"
``` 
Response
```
[
    {
        "examList": [
            {
                "firstname": "David",
                "id_exam": "7",
                "lastname": "Southern",
                "mark": "6.93"
            },
            {
                "firstname": "Jess",
                "id_exam": "8",
                "lastname": "Barrington",
                "mark": "5.11"
            },
            {
                "firstname": "Rachel",
                "id_exam": "1",
                "lastname": "Booker",
                "mark": "5.04"
            },
            {
                "firstname": "Ron",
                "id_exam": "11",
                "lastname": "Hopkins",
                "mark": "4.60"
            },
            {
                "firstname": "Marleen",
                "id_exam": "12",
                "lastname": "Heide",
                "mark": "3.91"
            },
            {
                "firstname": "Craig",
                "id_exam": "3",
                "lastname": "Johnson",
                "mark": "3.90"
            },
            {
                "firstname": "Martin",
                "id_exam": "10",
                "lastname": "Richardson",
                "mark": "3.77"
            },
            {
                "firstname": "Laura",
                "id_exam": "2",
                "lastname": "Grey",
                "mark": "3.74"
            },
            {
                "firstname": "Susan",
                "id_exam": "13",
                "lastname": "Roberts",
                "mark": "3.02"
            },
            {
                "firstname": "Mary",
                "id_exam": "4",
                "lastname": "Jenkins",
                "mark": "2.71"
            },
            {
                "firstname": "Fox",
                "id_exam": "6",
                "lastname": "Redmon",
                "mark": "2.56"
            },
            {
                "firstname": "Marion8",
                "id_exam": "14",
                "lastname": "Walker8",
                "mark": "1.88"
            }
        ]
    }
]
```
Post
```
curl -k --data '{"exam":{"firstName":"Giorgi","lastName":"Orts","mark":"9.99"}}'  -H "Content-Type: application/json" https://mage.dev.local/rest/V1/hiberus-orts
```
Response
```
{
    "exam_id": 23,
    "first_name": "Joss",
    "last_name": "Smith",
    "mark": 8
}
```

Delete
```
curl -k  -X DELETE https://mage.dev.local/rest/V1/hiberus-orts\?exam\[examId\]\=23
```
Response
```
{
    "exam_id": 23,
    "first_name": "Joss",
    "last_name": "Smith",
    "mark": 8
}
```

### Commands
Gets given number or all data from exams paged
 
```
php bin/magento hiberus:orts
php bin/magento hiberus:orts  --quantity="5"
```
![alt text](img/command.png "Command output")

### Tests

There are Integration tests and Unit tests so there are two configurable paths to run each type tests. The phpunit config files are stored inside the required test type.
```
/Integration/phpunit.xml
/Unit/phpunit.xml
``` 
In order to run integration tests we need to rename the .dist to `install-config-mysql.php` config in `dev/tests/integration/etc/install-config-mysql.php` and set database data. Be careful to do not name this database as the application database. First time we run tests it will create all the magento environment including database.
i.e.
```
return [
    'db-host' => '127.0.0.1:3306',
    'db-user' => 'user',
    'db-password' => 'password',
    'db-name' => 'magento_integration_tests',
    'db-prefix' => '',
    'backend-frontname' => 'backend',
    'admin-user' => \Magento\TestFramework\Bootstrap::ADMIN_NAME,
    'admin-password' => \Magento\TestFramework\Bootstrap::ADMIN_PASSWORD,
    'admin-email' => \Magento\TestFramework\Bootstrap::ADMIN_EMAIL,
    'admin-firstname' => \Magento\TestFramework\Bootstrap::ADMIN_FIRSTNAME,
    'admin-lastname' => \Magento\TestFramework\Bootstrap::ADMIN_LASTNAME,
];
```
Running tests:
I suggest to set up the config on PHPStorm as I used to do. But we also can run them from the command line:
Unit
```
path/bin/php path/vendor/phpunit/phpunit/phpunit --configuration path/Hiberus/Orts/Test/Integration/phpunit.xml path/Hiberus/Orts/Test/Unit
```
![alt text](img/unit.png "Unit tests ouput")


Integration
```
path/bin/php path/vendor/phpunit/phpunit/phpunit --configuration path/Hiberus/Orts/Test/Integration/phpunit.xml path/Hiberus/Orts/Test/Integration
```
![alt text](img/integration_one.png "Integration tests ouput")
![alt text](img/integration_two.png "Integration tests ouput")


## Screenshots
### Front (Url: [hostUrl]/orts)

#### List
![alt text](img/front_list_a.png "")

#### Higher Alert
![alt text](img/front_list_b.png "")

#### Hide Marks
![alt text](img/front_list_c.png "")

### Admin
#### Menu
![alt text](img/admin_menu.png "")

#### Ui Component Grid
![alt text](img/grid_actions.png "")

#### Mass Delete Action
![alt text](img/massdelete.png "")

#### Mass Delete Action Confirmation Alert
![alt text](img/delete_mass_ok.png "")

#### Ui Component Form Add
![alt text](img/form_a.png "")

#### Ui Component Form Confirmation Alert
![alt text](img/added.png "")

#### Ui Component Form Edit
![alt text](img/edit.png "")

#### Ui Component Form Edit Confirmation Alert
![alt text](img/edited_OK.png "")

#### Ui Component Form Single Delete
![alt text](img/single_delete.png "")

#### Ui Component Form Single Confirmation Alert
![alt text](img/single_delete_ok.png "")




