<?php
namespace Hiberus\Orts\Test\Integration\Model;

use Hiberus\Orts\Api\Data\ExamInterface;
use Hiberus\Orts\Model\Exam;
use Hiberus\Orts\Model\ExamRepository;
use Magento\Framework\Api\Filter;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;

/**
 * @author: Jose Manuel Orts
 */

class ExamRepositoryTest extends Testcase
{
    /**
     * @var string
    */
    protected $name;
    /**
     * @var string
     */
    protected $lastName;
    /**
     * @var int
     */
    protected $mark;
    /**
     * SUT
     * @var ExamRepository
     */
    protected $examRepository;

    public function setUp(): void
    {
        $this->examRepository = Bootstrap::getObjectManager()->create(
            ExamRepository::class
        );
    }

    public function testShouldSaveAnExam()
    {
        $createdExam = $this->createExam();

        static::assertInstanceOf(Exam::class, $createdExam);
        static::assertEquals($this->name, $createdExam->getFirstName());
        static::assertEquals($this->lastName, $createdExam->getLastName());
        static::assertEquals($this->mark, $createdExam->getMark());

        static::assertTrue($this->examRepository->delete($createdExam));
    }

    public function testShouldDeleteAnExam()
    {
        $createdExam = $this->createExam();
        $wasRemoved = $this->examRepository->delete($createdExam);
        self::assertTrue($wasRemoved);
    }

    public function testShouldGetHighestMark()
    {
        $searchCriteria = $this->getSearchCriteria();
        $searchResults = $this->examRepository->getArrayList($searchCriteria);

        $expectedHighestMark = max(array_column($searchResults->getItems(), 'mark'));

        $highestMark = $this->examRepository->getHighestMark();

        self::assertEquals($expectedHighestMark, $highestMark);
        self::assertTrue(is_float($highestMark));
    }

    public function testShouldGetMarkAverage()
    {
        /**@var SearchCriteriaBuilder $searchCriteriaBuilder*/
        $searchCriteria = $this->getSearchCriteria();
        $searchResults = $searchResults = $this->examRepository->getArrayList($searchCriteria);

        $items = $searchResults->getItems();

        $expectedAverage = array_sum(array_column($items, 'mark')) / count($items);

        $average = $this->examRepository->getAverage();

        self::assertEquals(number_format($expectedAverage, 2), number_format($average, 2));
        self::assertTrue(is_float($average));
    }

    public function testShouldLoadGivenExam()
    {
        $examId = 1;
        $expectedName = 'Rachel';
        $expectedLastName = 'Booker';

        $exam = $this->examRepository->load($examId);

        static::assertInstanceOf(Exam::class, $exam);
        static::assertEquals($expectedName, $exam->getFirstName());
        static::assertEquals($expectedLastName, $exam->getLastName());
    }

    public function testShouldGetFilteredList()
    {
        $pageSize = 5;
        $expectedFirstName = 'Dorian';
        $expectedFirstLastName = 'Fowler';

        /**@var SearchCriteriaBuilder $searchCriteriaBuilder*/
        $searchCriteriaBuilder = Bootstrap::getObjectManager()->create(SearchCriteriaBuilder::class);

        /**@var SortOrderBuilder $sorter */
        $sorter = Bootstrap::getObjectManager()->create(SortOrderBuilder::class);

        /**@var  Filter $filter */
        $filter = Bootstrap::getObjectManager()->create(Filter::class);

        /**@var FilterGroup $filterGroup */
        $filterGroup = Bootstrap::getObjectManager()->create(FilterGroup::class);

        $filter->setField('mark')
            ->setConditionType('gteq')
            ->setValue(5);
        $filterGroup->setFilters([$filter]);

        $sortOrder = $sorter->setField('mark')
            ->setDirection(SortOrder::SORT_DESC)
            ->create();

        /**@var SearchCriteria $searchCriteria*/
        $searchCriteria = $searchCriteriaBuilder->setPageSize($pageSize)
            ->setFilterGroups([$filterGroup])
            ->setSortOrders([$sortOrder])
            ->create();

        $searchResults = $this->examRepository->getList($searchCriteria);
        $items = $searchResults->getItems();

        self::assertEquals($pageSize, count($items));
        self::assertEquals($expectedFirstName, $items[0]['firstname']);
        self::assertEquals($expectedFirstLastName, $items[0]['lastname']);
    }

    /** internal library */

    protected function createExam(): ExamInterface
    {
        $this->name = 'Nikola';
        $this->lastName = 'Tesla';
        $this->mark = 10;

        $exam = Bootstrap::getObjectManager()
            ->create(
                Exam::class
            );

        $exam->setFirstName($this->name)
            ->setLastName($this->lastName)
            ->setMark($this->mark);

        return $this->examRepository->save($exam);
    }

    protected function getSearchCriteria(): SearchCriteria
    {
        /**@var SearchCriteriaBuilder $searchCriteriaBuilder */
        $searchCriteriaBuilder = Bootstrap::getObjectManager()->create(SearchCriteriaBuilder::class);
        /**@var SearchCriteria $searchCriteria */
        return $searchCriteriaBuilder->create();
    }
}
