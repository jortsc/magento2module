define(['jquery'], function($){
    "use strict";
    return function hideMarks()
    {
        let button = $("#hideMarks");
        button.click(function() {
            let markList = $(".mark");
            if (markList.css("visibility") === "visible") {
                markList.css("visibility", "hidden");
                button.text('Show Marks');
            } else {
                markList.css("visibility", "visible");
                button.text('Hide Marks');
            }
        });
    }
});
